package version1;

import java.awt.Color;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class ModoRGB_V1 {

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setBounds(0, 0, 600, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

        JPanel panel = new JPanel();
        panel.setBackground(new Color(0, 0, 0));
        
        // Hilo encargado de cambiar los colores del objeto
        new Thread(() -> {
            try {
                while (true) {
                    // Subida
                    modificarRojo(0, 255, panel); // Subir Rojo

                    modificarVerde(0, 255, panel); // Subir Verde
                    modificarRojo(255, 0, panel); // BajarRojo

                    modificarAzul(0, 255, panel); // Subir Azul
                    modificarVerde(255, 0, panel); // Bajar Verde

                    modificarRojo(0, 255, panel); // Subir Rojo
                    modificarVerde(0, 255, panel); // Subir Verde
                    Thread.sleep(100); // Para que el color blanco dure un poco más
                    // Bajada
                    modificarVerde(255, 0, panel); // Bajar Verde
                    modificarRojo(255, 0, panel); // Bajar Rojo

                    modificarVerde(0, 255, panel); // Subir Verde
                    modificarAzul(255, 0, panel); // Bajar Azul

                    modificarRojo(0, 255, panel); // Subir Rojo
                    modificarVerde(255, 0, panel); // Bajar Verde

                    modificarRojo(255, 0, panel); // Bajar Rojo
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(ModoRGB_V1.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        ).start();
        
        frame.add(panel);
    }

    /*
    |   Argumento 1: En que número empieza el bucle (Pensado para recivir 1 de los siguientes valores, 0 o 255)
    |   Argumento 2: Hasta que numero continua el bucle (Pensado para recivir 1 de los siguientes valores, 0 o 255)
    |   Argumento 3: El objeto al que se le va ha aplicar el modo RGB
    |
    |   Cuando el argumento 1 es = 0 el bucle avanza con paso +1, cuando no lo es (se supone que sera 255) avanza con paso -1
    |
    |   Donde pone "i != (fin + operador)" esta para que al argumento 2, le sume 1 o le reste 1 en funcion del primer argumento
    |   (para que el bucle recorra los 255 colores)
     */
    private static void modificarRojo(int inicio, int fin, JPanel panel) throws InterruptedException {
        int green = panel.getBackground().getGreen();
        int blue = panel.getBackground().getBlue();
        int operador = (inicio == 0) ? 1 : -1;
        for (int i = inicio; i != (fin + operador); i += operador) {
            panel.setBackground(new Color(i, green, blue));
            Thread.sleep(10);
        }
    }

    private static void modificarVerde(int inicio, int fin, JPanel panel) throws InterruptedException {
        int red = panel.getBackground().getRed();
        int blue = panel.getBackground().getBlue();
        int operador = (inicio == 0) ? 1 : -1;
        for (int i = inicio; i != (fin + operador); i += operador) {
            panel.setBackground(new Color(red, i, blue));
            Thread.sleep(10);
        }
    }

    private static void modificarAzul(int inicio, int fin, JPanel panel) throws InterruptedException {
        int red = panel.getBackground().getRed();
        int green = panel.getBackground().getGreen();
        int operador = (inicio == 0) ? 1 : -1;
        for (int i = inicio; i != (fin + operador); i += operador) {
            panel.setBackground(new Color(red, green, i));
            Thread.sleep(10);
        }
    }

}
