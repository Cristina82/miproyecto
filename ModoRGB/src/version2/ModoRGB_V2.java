package version2;

import java.awt.Color;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class ModoRGB_V2 {

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setBounds(0, 0, 600, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

        JPanel panel = new JPanel();
        panel.setBackground(new Color(0, 0, 0));
        
        // Hilo encargado de cambiar los colores del objeto
        new Thread(() -> {
            try {
                while (true) {
                    // Subida
                    modificarColor(0, 0, 255, panel); // Subir Rojo

                    modificarColor(1, 0, 255, panel); // Subir Verde
                    modificarColor(0, 255, 0, panel); // Bajar Rojo

                    modificarColor(2, 0, 255, panel); // Subir Azul
                    modificarColor(1, 255, 0, panel); // Bajar Verde

                    modificarColor(0, 0, 255, panel); // Subir Rojo
                    modificarColor(1, 0, 255, panel); // Subir Verde
                    Thread.sleep(100); // Para que el color blanco dure un poco más
                    // Bajada
                    modificarColor(1, 255, 0, panel); // Bajar Verde
                    modificarColor(0, 255, 0, panel); // Bajar Rojo

                    modificarColor(1, 0, 255, panel); // Subir Verde
                    modificarColor(2, 255, 0, panel); // Subir Azul

                    modificarColor(0, 0, 255, panel); // Subir Rojo
                    modificarColor(1, 255, 0, panel); // Bajar Verde

                    modificarColor(0, 255, 0, panel); // Subir Rojo
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(ModoRGB_V2.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        ).start();
        frame.add(panel);
    }

    /*
    |   Argumento 1: Número del color a modificar (0 = Rojo, 1 = Verde, 2 = Azul)
    |   Argumento 2: En que número empieza el bucle (Pensado para recibir 1 de los siguientes valores, 0 o 255)
    |   Argumento 3: Hasta que numero continua el bucle (Pensado para recibir 1 de los siguientes valores, 0 o 255)
    |   Argumento 4: El objeto al que se le va ha aplicar el modo RGB
    |
    |   Cuando el argumento 2 es = 0 el bucle avanza con paso +1, cuando no lo es (se supone que sera 255) avanza con paso -1
    |
    |   Donde pone "i != (fin + operador)" esta para que al argumento 3, le sume 1 o le reste 1 en funcion del segundo argumento
    |   (para que el bucle recorra las 255 partes del color a modificar)
     */
    private static void modificarColor(int numeroColor, int inicio, int fin, JPanel panel) throws InterruptedException {
        Color color = panel.getBackground();
        int colores[] = {color.getRed(), color.getGreen(), color.getBlue()};
        int operador = (inicio == 0) ? 1 : -1;
        for (int i = inicio; i != (fin + operador); i += operador) {
            colores[numeroColor] = i;
            panel.setBackground(new Color(colores[0], colores[1], colores[2]));
            Thread.sleep(10);
        }
    }
}
